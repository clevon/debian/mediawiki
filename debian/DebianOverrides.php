<?php
// Debian overrides for the MediaWiki installer.

$overrides['WebInstaller'] = DebianWebInstaller::class;

class DebianWebInstaller extends WebInstaller {
	public function getLocalSettingsLocation() {
		return '/etc/mediawiki/LocalSettings.php';
	}
}
