<?php
/**
 * Debian defaults for MediaWiki
 *
 * NEVER EDIT THIS FILE
 *
 * To customize your installation edit "LocalSettings.php".
 * See includes/DefaultSettings.php for more instructions and tips.
 */

$wgCacheDirectory = '/var/cache/mediawiki';

// Log exceptions, errors and fatals
$wgDBerrorLog = '/var/log/mediawiki/dberror.log';
$wgDebugLogGroups['exception'] = '/var/log/mediawiki/exception.log';
$wgDebugLogGroups['error'] = '/var/log/mediawiki/error.log';
$wgDebugLogGroups['fatal'] = '/var/log/mediawiki/fatal.log';
