Source: mediawiki
Section: web
Priority: optional
Maintainer: Raul Tambre <raul.tambre@clevon.com>
Homepage: https://www.mediawiki.org/
Vcs-Browser: https://gitlab.com/clevon/debian/mediawiki
Vcs-Git: https://gitlab.com/clevon/debian/mediawiki.git
Rules-Requires-Root: no
Standards-Version: 4.6.0
Build-Depends: debhelper-compat (= 13)

Package: mediawiki
Architecture: all
Multi-Arch: foreign
Depends:
 httpd | nginx,
 php-intl,
 php-json,
 php-mbstring,
 php-mysql | php-pgsql | php-sqlite3,
 php-xml,
 ${misc:Depends},
Recommends:
 php-apcu,
 php-cli,
 php-curl,
 php-gd,
 php-gmp,
 php-luasandbox,
 php-readline,
 php-wikidiff2,
 php-yaml,
 postgresql,
 python3,
Conflicts: mediawiki-classes
Description: Collaborative editing software that runs Wikipedia
 MediaWiki is a free and open-source wiki software package written in PHP. It
 serves as the platform for Wikipedia and the other Wikimedia projects, used by
 hundreds of millions of people each month. MediaWiki is localised in over 350
 languages and its reliability and robust feature set have earned it a large
 and vibrant community of third-party users and developers.
